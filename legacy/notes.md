#DE*sign* NOTES
- Don't do bootloader, use existing one, check if its higher-half kernel
- Understand modes, try doing some V86

Read up about multiboot
Add Stivale boot instead of multiboot

The following shows how to create a simple kernel in C. This kernel uses the VGA text mode buffer (located at 0xB8000) as the output device. It sets up a simple driver that remembers the location of the next character in this buffer and provides a primitive for adding a new character. Notably, there is no support for line breaks ('\n') (and writing that character will show some VGA-specific character instead) and no support for scrolling when the screen is filled up. Adding this will be your first task. Please take a few moments to understand the code.

IMPORTANT NOTE: the VGA text mode (as well as the BIOS) is deprecated on newer machines, and UEFI only supports pixel buffers. For forward compatibility you might want to start with that. Ask GRUB to set up a framebuffer using appropriate Multiboot flags or call VESA VBE yourself. Unlike VGA text mode, a framebuffer has pixels, so you have to draw each glyph yourself. This means you'll need a different terminal_putchar, and you'll need a font (bitmap images for each character). All Linux distro ships PC Screen Fonts that you can use, and the wiki article has a simple putchar() example. Otherwise everything else described here still stands (you have to keep track of the cursor position, implement line breaks and scrolling etc.)

#BUNCHA GOOD STUFF
https://wiki.osdev.org/Tutorials
https://wiki.osdev.org/Category:FAQ

#Testing
     Before activating a memory provider, ask it for memory, record the address, free the memory and ask for memory again. Most allocators are expected to return the very same address in this case. 

    If your memory manager has some 'free zone merging' feature, allocate as much 'small' zones as you can, then free all the 'odd' zones and then all the 'even' zones (stressing the merge feature). When you're done, try to allocate a single zone as large as the collection of all the small zones you initially get. 

    Before starting many threads, launch a thread that is expected to signal a semaphore and then wait on that semaphore. If your dispatcher (and semaphores) works fine, you can only go on if that other thread has been executed. 

    After enabling interrupts (and especially the timer), poll the 'tick counter' and don't go on until it advanced (confirming you that the timer interrupt works fine) 

As many of those tests may hang, you're suggested to print what test you're running before you know if the test was successful or not.

