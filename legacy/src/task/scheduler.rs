use core::cmp::Ordering;

use crossbeam_queue::ArrayQueue;
use priority_queue::PriorityQueue;

use super::{Scheduler, TaskId};

// TODO:
// enum SchedulerType {
//     RoundRobin,
//     Priority
// }
//
//
// impl SchedulerGeneric<> {
//     
// }


pub struct RoundRobinScheduler {
    task_queue: ArrayQueue<TaskId>,
}

impl RoundRobinScheduler {
    pub fn new() -> Self {
        return RoundRobinScheduler {
            task_queue: ArrayQueue::new(100),
        };
    }
}

unsafe impl Sync for RoundRobinScheduler {}
unsafe impl Send for RoundRobinScheduler {}

impl Scheduler for RoundRobinScheduler {
    fn add_task(&self, task_id: TaskId) {
        self.task_queue.push(task_id).expect("Task queue full");
    }

    fn remove_task(&self, task_id: TaskId) {
        todo!()
    }

    fn get_next_task(&self) -> Option<TaskId> {
        match self.task_queue.pop() {
            Ok(task_id) => Some(task_id),
            Err(_) => None,
        }
    }

    fn tasks_available(&self) -> bool {
        self.task_queue.is_empty()
    }
}

pub struct PriorityScheduler {
    task_queue: PriorityQueue<TaskId, Ordering, >
}
