use heapless::spsc::Queue;

use super::Task;

pub static mut SPAWNER_QUEUE: Queue<Task, 32> = Queue::new();

pub struct Spawner {}

impl Spawner {
    pub fn new() -> Self {
        Spawner {}
    }

    pub fn spawn_task(&self, task: Task) {
        let mut producer = unsafe { SPAWNER_QUEUE.split().0 };

        producer.enqueue(task);
    }
}
