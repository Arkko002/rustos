use alloc::boxed::Box;
use core::{
    future::Future,
    pin::Pin,
    task::{Context, Poll},
};

pub mod keyboard;
pub mod executor;
mod scheduler;
pub mod simple_executor;
pub mod channel;
pub mod spawner;

use core::sync::atomic::{AtomicU64, Ordering};


pub struct Task {
    id: TaskId,
    future: Pin<Box<dyn Future<Output = ()>>>,
}

unsafe impl Sync for Task {
    
}

impl Task {
    pub fn new(future: impl Future<Output = ()> + 'static) -> Task {
        Task {
            id: TaskId::new(),
            future: Box::pin(future),
        }
    }

    fn poll(&mut self, context: &mut Context) -> Poll<()> {
        self.future.as_mut().poll(context)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct TaskId(u64);

impl TaskId {
    fn new() -> Self {
        static NEXT_ID: AtomicU64 = AtomicU64::new(0);
        TaskId(NEXT_ID.fetch_add(1, Ordering::Relaxed))
    }
}

trait Scheduler {
     fn add_task(&self, task_id: TaskId);
    fn remove_task(&self, task_id: TaskId);

     fn get_next_task(&self) -> Option<TaskId>;

    fn tasks_available(&self) -> bool;
}
