use super::{scheduler::RoundRobinScheduler, spawner::SPAWNER_QUEUE, Scheduler, Task, TaskId};
use alloc::{collections::BTreeMap, sync::Arc, task::Wake};
use core::task::{Context, Poll, Waker};

pub struct Executor {
    tasks: BTreeMap<TaskId, Task>,
    scheduler: Arc<RoundRobinScheduler>,
    waker_cache: BTreeMap<TaskId, Waker>,
}

impl Executor {
    pub fn new() -> Self {
        Executor {
            tasks: BTreeMap::new(),
            scheduler: Arc::new(RoundRobinScheduler::new()),
            waker_cache: BTreeMap::new(),
        }
    }

    pub fn spawn(&mut self, task: Task) {
        let task_id = task.id;
        if self.tasks.insert(task.id, task).is_some() {
            panic!("task with same ID already in tasks");
        }
        self.scheduler.add_task(task_id);
    }

    pub fn run(&mut self) -> ! {
        loop {
            self.recieve_tasks();
            self.run_ready_tasks();
            self.sleep_if_idle();
        }
    }

    fn recieve_tasks(&mut self) {
        let mut consumer = unsafe { SPAWNER_QUEUE.split().1 };

        while let Some(task) = consumer.dequeue() {
            self.spawn(task);
        }
    }

    fn run_ready_tasks(&mut self) {
        // destructure `self` to avoid borrow checker errors
        let Self {
            tasks,
            scheduler,
            waker_cache,
        } = self;



        while let Some(task_id) = scheduler.get_next_task() {
            let task = match tasks.get_mut(&task_id) {
                Some(task) => task,
                None => continue, // task no longer exists
            };
            let waker = waker_cache
                .entry(task_id)
                .or_insert_with(|| TaskWaker::new(task_id, scheduler.clone()));
            let mut context = Context::from_waker(waker);
            match task.poll(&mut context) {
                Poll::Ready(()) => {
                    // task done -> remove it and its cached waker
                    tasks.remove(&task_id);
                    waker_cache.remove(&task_id);
                }
                Poll::Pending => {}
            }
        }
    }


    fn sleep_if_idle(&self) {
        use x86_64::instructions::interrupts::{self, enable_and_hlt};

        interrupts::disable();
        if self.scheduler.tasks_available() {
            enable_and_hlt();
        } else {
            interrupts::enable();
        }
    }
}

// TODO: Passing dyn Trait (scheduler) between threads
struct TaskWaker {
    task_id: TaskId,
    task_queue: Arc<RoundRobinScheduler>,
}

impl TaskWaker {
    fn new(task_id: TaskId, task_queue: Arc<RoundRobinScheduler>) -> Waker {
        Waker::from(Arc::new(TaskWaker {
            task_id,
            task_queue,
        }))
    }

    fn wake_task(&self) {
        self.task_queue.add_task(self.task_id);
    }
}

impl Wake for TaskWaker {
    fn wake(self: Arc<Self>) {
        self.wake_task();
    }

    fn wake_by_ref(self: &Arc<Self>) {
        self.wake_task();
    }
}
