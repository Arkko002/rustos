#![no_std]
//
// use core::sync::atomic::{AtomicPtr, Ordering};
// use core::ptr;
//
// struct Channel<T> {
//     sender: AtomicPtr<T>,
//     receiver: AtomicPtr<T>,
// }
//
// impl<T> Channel<T> {
//     fn new() -> Self {
//         Self {
//             sender: AtomicPtr::new(ptr::null_mut()),
//             receiver: AtomicPtr::new(ptr::null_mut()),
//         }
//     }
//
//     fn send(&self, data: T) {
//         let sender = self.sender.load(Ordering::Relaxed);
//         let receiver = self.receiver.load(Ordering::Relaxed);
//         if !receiver.is_null() {
//             unsafe {
//                 let data_ptr = Box::into_raw(Box::new(data));
//                 let mut next_sender = (data_ptr, sender);
//                 while let Err((new_data, next)) =
//                     core::ptr::NonNull::new(receiver).unwrap().as_ref().swap(&mut next_sender)
//                 {
//                     next_sender.0 = new_data;
//                     next_sender.1 = next;
//                 }
//                 self.sender.store(next_sender.0, Ordering::Relaxed);
//             }
//         } else {
//             panic!("no receiver found!");
//         }
//     }
//
//     fn recv(&self) -> Option<T> {
//         let sender = self.sender.load(Ordering::Relaxed);
//         let receiver = self.receiver.load(Ordering::Relaxed);
//         if !sender.is_null() {
//             unsafe {
//                 let mut next_receiver = (ptr::null_mut(), sender);
//                 while let Err((_, next)) =
//                     core::ptr::NonNull::new(sender).unwrap().as_ref().swap(&mut next_receiver)
//                 {
//                     next_receiver.1 = next;
//                 }
//                 let data = Box::from_raw(next_receiver.0);
//                 self.receiver.store(next_receiver.1, Ordering::Relaxed);
//                 Some(*data)
//             }
//         } else {
//             None
//         }
//     }
// }
//
// fn main() {
//     let channel = Channel::new();
//     let thread1 = core::thread::spawn(move || {
//         channel.send(42);
//     });
//     let thread2 = core::thread::spawn(move || {
//         let data = channel.recv();
//         println!("{}", data.unwrap()); // prints "42"
//     });
//     thread1.join().unwrap();
//     thread2.join().unwrap();
// }

