mod allocator;
mod r#virtual;

// TODO: Use virtual memory manager to be able to assign more memory addresses than physically
// possible

// https://wiki.osdev.org/Memory_Allocation

pub trait MemoryAllocator {
    fn allocate(start: usize, end: usize) -> u64;
    fn deallocate(block_index: u64) -> ();
}

pub struct MemoryRegion {
    start: u64,
    end: u64,
}
