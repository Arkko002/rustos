#![no_std]
#![no_main]

// TODO: https://crates.io/crates/bootloader

use bootloader_api::{entry_point, BootInfo};

mod memory;
mod video;


entry_point!(main);

fn main(boot_info: &'static mut BootInfo) -> ! {
  loop {
      
  }
}

// TODO: Print debug info using vga
#[panic_handler]
fn panic(_info: &core::panic::PanicInfo) -> ! {
  loop {}
}
