// use core::ptr::write_volatile;
//
// pub struct ColorMonitorTextOutput {
//     vga_buffer_addr: u16,
//     vga_buffer_addr_current: *mut u16,
//
//     bg_color: u8,
//     fg_color: u8,
//
//     cursor_x: u8,
//     cursor_y: u8,
//
//     cursor_x_max: u8,
//     cursor_y_max: u8,
// }
//
// // https://wiki.osdev.org/Printing_To_Screen
// impl ColorMonitorTextOutput {
//     pub fn new() -> Self {
//         return Self {
//             vga_buffer_addr: 0xB8000 as *mut u16,
//             vga_buffer_addr_current: 0xB8000 as *mut u16,
//             bg_color: 0x20,
//             fg_color: 0x07,
//             cursor_y: 0,
//             cursor_x: 0,
//             cursor_x_max: 80,
//             cursor_y_max: 25,
//         };
//     }
// }
//
// impl TextVideoOutput for ColorMonitorTextOutput {
//     // TODO: Newline, cursor movement
//     // TODO: On x>80 -> x=0, y>25 -> scroll y???
//     fn print(&self, text: &str) -> Result<(), Error> {
//         text.bytes().map(|char_byte| {
//             // write text to first byte 
//             unsafe { write_volatile(self.vga_buffer_addr, char_byte) }
//             // TODO:This pointer moving probably doesnt work in rust
//             self.vga_buffer_addr_current = self.vga_buffer_addr_current + 1;
//
//             // write attribute (color) to second byte
//             let color = self.fg_color + self.bg_color;
//             unsafe { write_volatile(self.vga_buffer_addr, color) }
//             self.vga_buffer_addr_current = self.vga_buffer_addr_current + 1;
//         });
//
//         Ok(())
//     }
//
//     fn println(&self, text: &str) -> Result<(), Error> {
//         todo!()
//     }
// }
