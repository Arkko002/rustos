// use core::error::Error;
//
// pub mod factory;
// mod color;
// mod monochrome;
//
// pub trait TextVideoOutput {
//     fn print(&self, text: &str) -> Result<(), Error>;
//     fn println(&self, text: &str) -> Result<(), Error>;
//     fn read_buffer(&self) -> Result<String, Error>;
// }
//
// // TODO: panic! for unrecoverable errors
// #[derive(Debug, Clone)]
// pub struct TextOutputError {
//     message: &str,
// }
//
// impl TextOutputError {
//     pub fn new(message: &str) -> TextOutputError {
//         TextOutputError { message }
//     }
// }
//
// impl Display for TextOutputError {
//     fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
//         write!(f, "{}", self.message)
//     }
// }
//
// impl Error for TextOutputError {}
